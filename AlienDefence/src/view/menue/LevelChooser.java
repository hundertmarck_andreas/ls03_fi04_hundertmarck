package view.menue;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;
import java.awt.Color;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;

	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 */
	public LevelChooser(AlienDefenceController alienDefenceController, LeveldesignWindow leveldesignWindow, User user, String description) {
		setBackground(Color.BLACK);
		this.lvlControl = alienDefenceController.getLevelController();
		//this.lvlControl = lvlControl;
		this.leveldesignWindow = leveldesignWindow;

		setLayout(new BorderLayout());

		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		add(pnlButtons, BorderLayout.SOUTH);

		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.setForeground(Color.ORANGE);
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnSpielen_Clicked(alienDefenceController, user);
			}
		});
		pnlButtons.add(btnSpielen);
		
		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.setForeground(Color.ORANGE);
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		pnlButtons.add(btnNewLevel);

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.setForeground(Color.ORANGE);
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.setForeground(Color.ORANGE);
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);
		
		JButton btnHighscore = new JButton("Highscore");
		btnHighscore.setForeground(Color.ORANGE);
		btnHighscore.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//Dropdown entfernt --- muss angepasst werden!
				new Highscore(alienDefenceController.getAttemptController(), (1));
			}
		});
		pnlButtons.add(btnHighscore);

		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setForeground(Color.GREEN);
		lblLevelauswahl.setFont(new Font("Arial", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
		spnLevels.setForeground(Color.ORANGE);
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblLevels.setForeground(Color.ORANGE);
		tblLevels.setBackground(Color.BLACK);
		spnLevels.setViewportView(tblLevels);

		this.updateTableData();
		
		if(description.equals("Testen")) {
			btnNewLevel.setVisible(false);
			btnUpdateLevel.setVisible(false);
			btnDeleteLevel.setVisible(false);
		}else if(description.equals("Leveleditor")) {
			btnSpielen.setVisible(false);
			btnHighscore.setVisible(false);
		}
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
	
	public void btnSpielen_Clicked(AlienDefenceController alienDefenceController, User user) {
        //Level_id des selektierten Elements auslesen
        int level_id = Integer
                .parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
        
        //gewähltes Level aus der Persistenz holen
        Level level = alienDefenceController.getLevelController().readLevel(level_id);
        
        //Gameprozess starten
        Thread t = new Thread("GameThread") {

            @Override
            public void run() {

                // Spielaufruf durchführen
                GameController gameController = alienDefenceController.startGame(level, user);
                new GameGUI(gameController).start();

            }
        };
        //Prozess starten
        t.start();
        //Levelauswahlfenster schließen
        this.leveldesignWindow.dispose();
	}
}
