package view;

import javax.swing.JColorChooser;
import java.awt.Font;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Insets;
import java.awt.Component;
import java.awt.Toolkit;
import java.awt.LayoutManager;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JFrame;
import java.awt.BorderLayout;
import java.awt.EventQueue;


public class FormAendern extends JFrame {

	private JPanel contentPane;
	private JLabel lbl_anzeige;
    private JLabel lbl_aufgabe1;
    private JButton btn_rot;
    private JButton btn_gruen;
    private JButton btn_blau;
    private JButton btn_gelb;
    private JButton btn_standard;
    private JButton btn_farbe_auswaehlen;
    private JLabel lbl_aufgabe2;
    private JButton btn_arial;
    private JButton btn_comic;
    private JButton btn_courier;
    private JTextField tfd_eingabe;
    private JButton btn_label_aendern;
    private JButton btn_label_loeschen;
    private JLabel lbl_aufgabe3;
    private JButton btn_schrift_rot;
    private JButton btn_schrift_blau;
    private JButton btn_schrift_schwarz;
    private JLabel lbl_aufgabe4;
    private JButton btn_plus;
    private JButton btn_minus;
    private JLabel lbl_aufgabe5;
    private JButton btn_links;
    private JButton btn_zentriert;
    private JButton btn_rechts;
    private JLabel lbl_aufgabe6;
    private JButton btn_beenden;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FormAendern frame = new FormAendern();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public FormAendern() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 470, 640);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lbl_anzeige = new JLabel("Dieser Text soll ver\u00E4ndert werden.");
		lbl_anzeige.setBounds(123, 16, 251, 44);
		contentPane.add(lbl_anzeige);
		
		JLabel lbl_aufgabe1 = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lbl_aufgabe1.setBounds(0, 72, 308, 20);
		contentPane.add(lbl_aufgabe1);
		
		JButton btn_rot = new JButton("Rot");
		btn_rot.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_rot_ActionPerformed(evt);
			}
		});
		btn_rot.setBounds(10, 95, 141, 29);
		contentPane.add(btn_rot);
		
		JButton btn_blau = new JButton("Blau");
		btn_blau.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_blau_ActionPerformed(evt);
			}
		});
		btn_blau.setBounds(155, 95, 141, 29);
		contentPane.add(btn_blau);
		
		JButton btn_gruen = new JButton("Gr\u00FCn");
		btn_gruen.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_gruen_ActionPerformed(evt);
			}
		});
		btn_gruen.setBounds(299, 95, 141, 29);
		contentPane.add(btn_gruen);
		
		JButton btn_gelb = new JButton("Gelb");
		btn_gelb.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_gelb_ActionPerformed(evt);
			}
		});
		btn_gelb.setBounds(10, 130, 141, 29);
		contentPane.add(btn_gelb);
		
		JButton btn_standard = new JButton("Standardfarbe");
		btn_standard.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_standard_ActionPerformed(evt);
			}
		});
		btn_standard.setBounds(155, 130, 141, 29);
		contentPane.add(btn_standard);
		
		JButton btn_farbe_auswaehlen = new JButton("Farbe w\u00E4hlen");
		btn_farbe_auswaehlen.addActionListener(new ActionListener() {
			@Override
	        public void actionPerformed(final ActionEvent evt) {
	            FormAendern.this.btn_farbe_auswaehlen_ActionPerformed(evt);
	        }
		});
		btn_farbe_auswaehlen.setBounds(299, 130, 141, 29);
		contentPane.add(btn_farbe_auswaehlen);
		
		JLabel lbl_aufgabe2 = new JLabel("Aufgabe 2: Text formatieren");
		lbl_aufgabe2.setBounds(0, 175, 308, 20);
		contentPane.add(lbl_aufgabe2);
		
		JButton btn_arial = new JButton("Arial");
		btn_arial.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_arial_ActionPerformed(evt);
            }
		});
		btn_arial.setBounds(10, 199, 141, 29);
		contentPane.add(btn_arial);
		
		JButton btn_comic = new JButton("Comic Sans MS");
		btn_comic.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_comic_ActionPerformed(evt);
            }
		});
		btn_comic.setBounds(155, 199, 141, 29);
		contentPane.add(btn_comic);
		
		JButton btn_courier = new JButton("Courier New");
		btn_courier.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_courier_ActionPerformed(evt);
            }
		});
		btn_courier.setBounds(299, 199, 141, 29);
		contentPane.add(btn_courier);
		
		tfd_eingabe = new JTextField();
		tfd_eingabe.setText("Hier bitte Text eingeben");
		tfd_eingabe.setBounds(10, 233, 430, 26);
		contentPane.add(tfd_eingabe);
		tfd_eingabe.setColumns(10);
		
		JButton btn_lbl_aendern = new JButton("Ins Label schreiben");
		btn_lbl_aendern.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_label_aendern_ActionPerformed(evt);
            }
		});
		btn_lbl_aendern.setBounds(10, 269, 210, 29);
		contentPane.add(btn_lbl_aendern);
		
		JButton btn_lbl_loeschen = new JButton("Text im Label l\u00F6schen");
		btn_lbl_loeschen.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_label_loeschen_ActionPerformed(evt);
            }
		});
		btn_lbl_loeschen.setBounds(230, 269, 210, 29);
		contentPane.add(btn_lbl_loeschen);
		
		JLabel lbl_aufgabe3 = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lbl_aufgabe3.setBounds(0, 314, 308, 20);
		contentPane.add(lbl_aufgabe3);
		
		JButton btn_schrift_rot = new JButton("Rot");
		btn_schrift_rot.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_schrift_rot_ActionPerformed(evt);
            }
		});
		btn_schrift_rot.setBounds(10, 337, 141, 29);
		contentPane.add(btn_schrift_rot);
		
		JButton btn_schrift_blau = new JButton("Blau");
		btn_schrift_blau.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_schrift_blau_ActionPerformed(evt);
            }
		});
		btn_schrift_blau.setBounds(155, 337, 141, 29);
		contentPane.add(btn_schrift_blau);
		
		JButton btn_schrift_schwarz = new JButton("Schwarz");
		btn_schrift_schwarz.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_schrift_schwarz_ActionPerformed(evt);
            }
		});
		btn_schrift_schwarz.setBounds(299, 337, 141, 29);
		contentPane.add(btn_schrift_schwarz);
		
		JLabel lbl_aufgabe4 = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe ver\u00E4ndern");
		lbl_aufgabe4.setBounds(0, 382, 296, 20);
		contentPane.add(lbl_aufgabe4);
		
		JButton btn_plus = new JButton("+");
		btn_plus.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_plus_ActionPerformed(evt);
            }
		});
		btn_plus.setBounds(10, 403, 210, 29);
		contentPane.add(btn_plus);
		
		JButton btn_minus = new JButton("-");
		btn_minus.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_minus_ActionPerformed(evt);
            }
		});
		btn_minus.setBounds(230, 403, 210, 29);
		contentPane.add(btn_minus);
		
		JLabel lbl_aufgabe5 = new JLabel("Aufgabe 5: Textausrichtung");
		lbl_aufgabe5.setBounds(0, 448, 296, 20);
		contentPane.add(lbl_aufgabe5);
		
		JButton btn_links = new JButton("linksb\u00FCndig");
		btn_links.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_links_ActionPerformed(evt);
            }
		});
		btn_links.setBounds(10, 471, 141, 29);
		contentPane.add(btn_links);
		
		JButton btn_zentriert = new JButton("zentriert");
		btn_zentriert.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_zentriert_ActionPerformed(evt);
            }
		});
		btn_zentriert.setBounds(155, 471, 141, 29);
		contentPane.add(btn_zentriert);
		
		JButton btn_rechts = new JButton("rechtsb\u00FCndig");
		btn_rechts.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_rechts_ActionPerformed(evt);
            }
		});
		btn_rechts.setBounds(299, 471, 141, 29);
		contentPane.add(btn_rechts);
		
		JLabel lbl_aufgabe6 = new JLabel("Aufgabe 6: Programm Beenden");
		lbl_aufgabe6.setBounds(0, 516, 296, 20);
		contentPane.add(lbl_aufgabe6);
		
		JButton btn_beenden = new JButton("Beenden");
		btn_beenden.addActionListener(new ActionListener() {
			@Override
            public void actionPerformed(final ActionEvent evt) {
                FormAendern.this.btn_beenden_ActionPerformed(evt);
            }
		});
		btn_beenden.setBounds(10, 539, 430, 45);
		contentPane.add(btn_beenden);
	}

	public void btn_rot_ActionPerformed(final ActionEvent evt) {
		this.contentPane.setBackground(Color.RED);
	}
	public void btn_blau_ActionPerformed(final ActionEvent evt) {
		this.contentPane.setBackground(Color.BLUE);
	}
	public void btn_gruen_ActionPerformed(final ActionEvent evt) {
		this.contentPane.setBackground(Color.GREEN);
	}
	public void btn_gelb_ActionPerformed(final ActionEvent evt) {
		this.contentPane.setBackground(Color.YELLOW);
	}
	public void btn_standard_ActionPerformed(final ActionEvent evt) {
		this.contentPane.setBackground(new Color(0xEEEEEE));
	}
	public void btn_farbe_auswaehlen_ActionPerformed(final ActionEvent evt) {
		this.contentPane.setBackground(this.chooseColor());
	}
	public Color chooseColor() {
	    return JColorChooser.showDialog(this, "W\u00e4hle Farbe", Color.white);
	}
	public void btn_arial_ActionPerformed(final ActionEvent evt) {
        this.lbl_anzeige.setFont(new Font("Arial", 0, this.lbl_anzeige.getFont().getSize()));
    }
	public void btn_comic_ActionPerformed(final ActionEvent evt) {
        this.lbl_anzeige.setFont(new Font("Comic Sans MS", 0, this.lbl_anzeige.getFont().getSize()));
    }
	public void btn_courier_ActionPerformed(final ActionEvent evt) {
        this.lbl_anzeige.setFont(new Font("Courier New", 0, this.lbl_anzeige.getFont().getSize()));
    }
	public void btn_label_aendern_ActionPerformed(final ActionEvent evt) {
        this.lbl_anzeige.setText(this.tfd_eingabe.getText());
    }
	public void btn_label_loeschen_ActionPerformed(final ActionEvent evt) {
        this.lbl_anzeige.setText("");
    }
	public void btn_schrift_rot_ActionPerformed(final ActionEvent evt) {
        this.lbl_anzeige.setForeground(Color.RED);
    }
	public void btn_schrift_blau_ActionPerformed(final ActionEvent evt) {
        this.lbl_anzeige.setForeground(Color.BLUE);
    }
	public void btn_schrift_schwarz_ActionPerformed(final ActionEvent evt) {
        this.lbl_anzeige.setForeground(Color.BLACK);
    }
	public void btn_plus_ActionPerformed(final ActionEvent evt) {
        final String aktuellerFont = this.lbl_anzeige.getFont().getFontName();
        final int groesse = this.lbl_anzeige.getFont().getSize();
        this.lbl_anzeige.setFont(new Font(aktuellerFont, 0, groesse + 1));
    }
	public void btn_minus_ActionPerformed(final ActionEvent evt) {
        final String aktuellerFont = this.lbl_anzeige.getFont().getFontName();
        int groesse = this.lbl_anzeige.getFont().getSize();
        if (groesse > 1) {
            --groesse;
        }
        this.lbl_anzeige.setFont(new Font(aktuellerFont, 0, groesse));
    }
	public void btn_links_ActionPerformed(final ActionEvent evt) {
        this.lbl_anzeige.setHorizontalAlignment(2);
    }
	public void btn_zentriert_ActionPerformed(final ActionEvent evt) {
        this.lbl_anzeige.setHorizontalAlignment(0);
    }
	public void btn_rechts_ActionPerformed(final ActionEvent evt) {
        this.lbl_anzeige.setHorizontalAlignment(4);
    }
	public void btn_beenden_ActionPerformed(final ActionEvent evt) {
        System.exit(1);
    }
	
}
